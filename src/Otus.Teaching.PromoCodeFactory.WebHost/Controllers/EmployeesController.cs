﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        [HttpPost]        
        public async Task<IActionResult> Create(NewEmployeeRequest employee) 
        {
            if (employee == null || !employee.Validate()) return BadRequest();

            var availableRoles = await _rolesRepository.GetAllAsync();
            if (!employee.Roles.All(r => availableRoles.Any(a => a.Id == r))) return BadRequest();

            await _employeeRepository.Create(new Employee
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Roles = employee.Roles.Select(r => availableRoles.Single(a => a.Id == r)).ToList()
            });
            return Ok();

        }

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="id">идентификатор пользователя</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _employeeRepository.Delete(id);

            if (result)
                return Ok();
            else
                return NotFound();

        }

        /// <summary>
        /// Обновление пользователя
        /// </summary>
        /// <param name="id">идентификатор пользователя</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update(UpdatedEmployeeRequest employee)
        {
            if (employee == null || !employee.Validate()) return BadRequest();

            var availableRoles = await _rolesRepository.GetAllAsync();
            if (!employee.Roles.All(r => availableRoles.Any(a => a.Id == r))) return BadRequest();

            var result = await _employeeRepository.Update(new Employee
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Roles = employee.Roles.Select(r => availableRoles.Single(a => a.Id == r)).ToList()
            });

            if (result)
                return Ok();
            else
                return NotFound();

        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}