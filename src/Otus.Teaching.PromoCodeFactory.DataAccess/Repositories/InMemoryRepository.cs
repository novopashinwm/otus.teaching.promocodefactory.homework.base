﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task Create(T employe) 
        {
            employe.Id = Guid.NewGuid();
            Data.Add(employe);
            return Task.CompletedTask;
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> Update(T employe)
        {
            var list = Data.ToList();
            var oldItem = list.FirstOrDefault(x => x.Id == employe.Id);
            if (oldItem == null) return Task.FromResult(false);
            var index = list.IndexOf(oldItem);
            list[index] = employe;
            Data = list;
            return Task.FromResult(true);
        }

        public Task<bool> Delete(Guid id)
        {
            var entityToUpdate = Data.SingleOrDefault(e => e.Id == id);
            if (entityToUpdate == null) return Task.FromResult(false);

            var index = Data.IndexOf(entityToUpdate);
            Data.RemoveAt(index);
            return Task.FromResult(true);

        }
    }
}